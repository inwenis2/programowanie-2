package date_05082018.tictactoe;

public class Board {

    private MARK[][] boardArray;

    public Board() {
        boardArray = new MARK[3][3];
    }

    public MARK[][] createBoard() {
        return boardArray = new MARK[3][3];
    }

    public boolean isClear() {
        for (int i = 0; i < boardArray.length; i++) {
            for (int j = 0; j < boardArray[i].length; j++) {
                if (isFilled(i,j)) {
                    return false;
                }
            }
        }
        return true;
    }

    public void makeMove(MARK symbol, int a, int b) {
        isLegalArgument(a, b);
        if(isFilled(a,b)){
            throw new IllegalStateException();
        }
        boardArray[a][b] = symbol;
    }

    public MARK[][] getBoardArray() {
        return boardArray;
    }

    public MARK getValue(int a, int b){
        isLegalArgument(a, b);
        return boardArray[a][b];
    }

    private void isLegalArgument(int a, int b) {
        if (a < 0 || a >= boardArray.length || b < 0 || b >= boardArray[0].length) {
            throw new IllegalArgumentException();
        }
    }
    private boolean isFilled(int a, int b){
        return boardArray[a][b] != null;
    }
}
