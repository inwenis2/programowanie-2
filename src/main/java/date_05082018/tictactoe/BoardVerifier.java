package date_05082018.tictactoe;

import java.util.Arrays;

public class BoardVerifier {

    public static boolean isBoardResolved(Board board) {
        if (board.getBoardArray().length > 3) {
            throw new IllegalStateException("Nie można zweryfikować planszy innej niż o wielkości 3x3");
        } else {
            return !board.isClear() && (isRowResolved(board) || isColumnResolved(board) || isDiagonalResolved(board));
        }
    }

    public static boolean isRowResolved(Board board) {
        for (int row = 0; row < board.getBoardArray().length; row++) {
            if (board.getValue(row, 0) == board.getValue(row, 1) && board.getValue(row, 1) == board.getValue(row, 2)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isColumnResolved(Board board) {
        for (int column = 0; column < board.getBoardArray().length; column++) {
            if (board.getValue(0, column) == board.getValue(1, column) && board.getValue(1, column) == board.getValue(2, column)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isDiagonalResolved(Board board) {
        if (board.getValue(0, 0) == board.getValue(1, 1) && board.getValue(1, 1) == board.getValue(2, 2)) {
            return true;
        } else if (board.getValue(0, 2) == board.getValue(1, 1) && board.getValue(1, 1) == board.getValue(2, 0)) {
            return true;
        }
        return false;
    }

/*    public static boolean isRowResolved(Board board) {
        MARK[] rowX = {MARK.X, MARK.X, MARK.X};
        MARK[] rowO = {MARK.O, MARK.O, MARK.O};
        for (MARK[] row : board.getBoardArray()) {
            if (Arrays.equals(rowX, row) || Arrays.equals(rowO, row)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isColumnResolved(Board board) {
        MARK[] rowX = {MARK.X, MARK.X, MARK.X};
        MARK[] rowO = {MARK.O, MARK.O, MARK.O};
        MARK[] columnArray = new MARK[board.getBoardArray().length];
        for (int i = 0; i < board.getBoardArray().length; i++) {
            for (int j = 0; j < board.getBoardArray()[i].length; j++) {
                columnArray[j] = board.getBoardArray()[j][i];
            }
            if (Arrays.equals(rowX, columnArray) || Arrays.equals(rowO, columnArray)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isDiagonalResolved(Board board) {
        MARK[] rowX = {MARK.X, MARK.X, MARK.X};
        MARK[] rowO = {MARK.O, MARK.O, MARK.O};
        MARK[] diagonalArray = new MARK[board.getBoardArray().length];
        for (int i = 0; i < board.getBoardArray().length; i++) {
            diagonalArray[i] = board.getBoardArray()[i][i];
        }
        if (Arrays.equals(rowX, diagonalArray) || Arrays.equals(rowO, diagonalArray)) {
            return true;
        }
        int j = 0;
        for (int i = board.getBoardArray().length-1; i >= 0; i--) {
            diagonalArray[i] = board.getBoardArray()[j][i];
            j++;
        }
        if (Arrays.equals(rowX, diagonalArray) || Arrays.equals(rowO, diagonalArray)) {
            return true;
        }
        return false;
    }*/
}

