package date_21072018;

public class Palindrom {

    public static void main(String[] args) {
        isPalindrom("ala");
    }

    public static boolean isPalindrom(String string) {
        if (string == null || string.equals("")) {
            throw new IllegalArgumentException("Słowo nie może być puste lub nullem");
        }
        String a = string.toLowerCase().replaceAll(" ", "");
        String[] tab = a.split("");
        for (int i = 0; i < tab.length; i++) {
            if (!(tab[i].equals(tab[tab.length - 1 - i]))) {
                return false;
            }
        }
        return true;
    }
}
