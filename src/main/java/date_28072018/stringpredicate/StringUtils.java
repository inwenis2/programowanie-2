package date_28072018.stringpredicate;

public class StringUtils {

    public static String betterString(String s1, String s2, TwoStringPredicate predicate){
        if(predicate.compare(s1,s2)){
            return s1;
        } else {
            return s2;
        }
    }
}
