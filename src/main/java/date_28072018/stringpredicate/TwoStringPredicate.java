package date_28072018.stringpredicate;

public interface TwoStringPredicate {
    boolean compare(String s1, String s2);
}
