package date_28072018.stringpredicate;

public class Runner {
    public static void main(String[] args) {
        String str1 = "ala";
        String str2 = "elzbieta";

        System.out.println(StringUtils.betterString(str1, str2,(s1, s2) -> s1.length() > s2.length()));
        System.out.println(StringUtils.betterString(str1, str2,(s1, s2) -> true));
    }
}
