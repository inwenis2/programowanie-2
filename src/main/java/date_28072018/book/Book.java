package date_28072018.book;

import java.util.Objects;

public class Book {
    private String author;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return price == book.price &&
                releaseDate == book.releaseDate &&
                Objects.equals(author, book.author) &&
                Objects.equals(title, book.title);
    }

    @Override
    public int hashCode() {

        return Objects.hash(author, title, price, releaseDate);
    }

    private String title;
    private int price;
    private int releaseDate;

    public Book(String author, String title, int price, int releaseDate) {
        this.author = author;
        this.title = title;
        this.price = price;
        this.releaseDate = releaseDate;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }

    public int getReleaseDate() {
        return releaseDate;
    }
}
