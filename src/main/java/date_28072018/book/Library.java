package date_28072018.book;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Library {
    private List<Book> bookList;

    public Library(List<Book> bookList){
        if(bookList == null || bookList.isEmpty()){
            throw new IllegalArgumentException("Lista nie może być pusta");
        } else{
            this.bookList = bookList;
        }
    }

    public List<String> getAuthor(){
        return bookList.stream()
                .map(x -> x.getAuthor())
                .collect(Collectors.toList());
    }

    public List<String> getTitles(){
        return bookList.stream()
                .map(x -> x.getTitle())
                .collect(Collectors.toList());
    }

    public List<Integer> getPrices(){
        return bookList.stream()
                .map(x -> x.getPrice())
                .collect(Collectors.toList());
    }

    public List<Book> booksOfParticularAuthor(String author){
        return bookList.stream()
                .filter(x -> x.getAuthor().equals(author))
                .collect(Collectors.toList());
    }

    public List<Book> booksOfParticularTitle(String title){
        return bookList.stream()
                .filter(x -> x.getTitle().equals(title))
                .collect(Collectors.toList());
    }
    public List<Book> booksOfParticularPrice(int price){
        return bookList.stream()
                .filter(x -> x.getPrice()== price)
                .collect(Collectors.toList());
    }

    public List<Book> booksOfPriceRange(int from, int to){
        return bookList.stream()
                .filter(x -> x.getPrice() <= to && x.getPrice() >= from)
                .collect(Collectors.toList());
    }

    public int getPriceSum(){
        return bookList.stream()
                .map(x -> x.getPrice())
                .reduce(0,(x1, x2) -> x1 + x2);
    }

    //druga wersja metody wyżej
    public int getPriceSum2(){
        return bookList.stream()
                .mapToInt(x -> x.getPrice())
                .sum();
    }

    public int getPriceSumByAnAuthor(String author, String title){
        return bookList.stream()
                .filter(x -> x.getAuthor().equals(author))
                .filter(x -> !x.getTitle().equals(title))
                .mapToInt(x -> x.getPrice())
                .sum();
    }

    public List<Book> booksByAYear(int year){
        return bookList.stream()
                .filter(x -> x.getReleaseDate() == year)
                .collect(Collectors.toList());
    }

    public List<Book> booksByAnAuthorAndYearMaxPrice(String author, int year, int maxPrice){
        return bookList.stream()
                .filter(x -> x.getAuthor().equals(author))
                .filter(x -> x.getReleaseDate() == year)
                .filter(x -> x.getPrice() <= maxPrice)
                .collect(Collectors.toList());
    }

    public String getAuthorsInAParticularWay(){
        return bookList.stream()
                .map(x -> x.getAuthor())
                .reduce("- ",(x1, x2) -> x1 + x2 + "\n- ");
    }
}
