package date_28072018.observer;

import java.util.Observable;

public class Dispatcher extends Observable {

    public void accidentHappened(){
        System.out.println("Wydarzył się wypadek");
        setChanged();
        notifyObservers();
    }
}
