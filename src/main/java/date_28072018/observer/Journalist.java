package date_28072018.observer;

import java.util.Observable;
import java.util.Observer;

public class Journalist implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Robię relację z wypadku");
    }
}
