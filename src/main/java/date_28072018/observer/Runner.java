package date_28072018.observer;

public class Runner {
    public static void main(String[] args) {
        Dispatcher dispatcher = new Dispatcher();
        Ambulance ambulance = new Ambulance();
        Journalist journalist = new Journalist();
        CIAAgent ciaAgent = new CIAAgent();

        dispatcher.addObserver(ambulance);
        dispatcher.addObserver(journalist);
        dispatcher.addObserver(ciaAgent);

        dispatcher.accidentHappened();
    }
}
