package date_28072018.observer;

import java.util.Observable;
import java.util.Observer;

public class Ambulance implements Observer {
    Dispatcher dispatcher;
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Jadę do wypadku");
    }
}
