package date_28072018.elementpredicate;

public interface TwoElementPredicate <E> {
    public boolean compare(E e1, E e2);
}
