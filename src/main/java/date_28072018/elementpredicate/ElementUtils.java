package date_28072018.elementpredicate;

public class ElementUtils {
    public static <E> E betterEntry(E e1, E e2, TwoElementPredicate<E> predicate){
        if(predicate.compare(e1,e2)){
            return e1;
        } else {
            return e2;
        }
    }
}
