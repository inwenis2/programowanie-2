package date_28072018.elementpredicate;

public class Runner {
    public static void main(String[] args) {

        Car car1 = new Car(10);
        Car car2 = new Car(20);

        System.out.println(ElementUtils.betterEntry("ala", "elzbieta", ((e1, e2) -> e1.length() > e2.length())));
        //Wyrażenia lambda zastępuje tak naprawdę tworzenie anonimowej klasy z metodą o podanej logice.
        //Można to zapisać również tak: New TwoElementPredicate({
        // @Override
        // public boolean compare(String e1, String e2){
        // return e1.length() > e2.length()}});
        System.out.println(ElementUtils.betterEntry(car1, car2, ((e1, e2) -> e1.getPrice() > e2.getPrice())));
    }
}
