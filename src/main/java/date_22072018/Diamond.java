package date_22072018;

public class Diamond {
    public static void main(String[] args) {
        System.out.println(printDiamond("D"));
    }

    //ASCII A - Z : 65 - 90
    public static String printDiamond(String string) {
        char buildingLetter = string.toUpperCase().charAt(string.length() - 1);
        StringBuilder diamond = new StringBuilder();
        for (int i = buildingLetter - 'A'; i >= 0; i--) {
            buildDiamond(buildingLetter, diamond, i);
        }
        if (buildingLetter - 'A' == 0) {
            diamond.append("A");
        } else {
            for (int i = 1; i <= buildingLetter - 'A'; i++) {
                buildDiamond(buildingLetter, diamond, i);
            }
            diamond.deleteCharAt(diamond.length() - 1);
        }
        return diamond.toString();
    }

    private static void buildDiamond(char buildingLetter, StringBuilder sb, int i) {
        addSpace(sb, i).append((char)(buildingLetter - i));
        if (buildingLetter - 'A' - i > 0) {
            addSpace(sb, (buildingLetter - 'A' - i) * 2 - 1).append((char)(buildingLetter - i));
        }
        sb.append("\n");
    }

    private static StringBuilder addSpace(StringBuilder sb, int space) {
        for (int i = 0; i < space; i++) {
            sb.append(" ");
        }
        return sb;
    }
}
