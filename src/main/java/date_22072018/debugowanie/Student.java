package date_22072018.debugowanie;

public class Student extends Person {
    private String grade;
    private String daysAttended;

    public Student(String name, String grade, String daysAttended) {
        super(name);
        this.grade = grade;
        this.daysAttended = daysAttended;
    }

    public void inflateGrade(){
        this.grade = this.grade.replace("-", "+");
    }

    public void boostAttendance(){
        this.daysAttended = Integer.toString(Integer.parseInt(this.daysAttended) + 2);
    }

    @Override
    public String toString(){
        return "[Name: " + this.getName() + ", Grade: "+this.grade+ ", Days attended; " + this.daysAttended + "]";
    }
}
