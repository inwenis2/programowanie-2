package date_22072018.debugowanie;

import java.util.ArrayList;

public class RemovePeople {
    public static void main(String[] args) {
        Person ben = new Person("Ben");
        ArrayList<Person> persons = new ArrayList<>();
        persons.add(new Person(new String("Ben")));
        persons.add(new Person(new String("Alyssa")));
        persons.add(new Person(new String("Alice")));
        ArrayList<Person> toRemove = new ArrayList<>();
        for (Person person : persons){
            if (person.hasSameName(ben)) {
                toRemove.add(person);
            }
        }
        persons.removeAll(toRemove);
        System.out.println(persons);

        Student student1 = new Student("Ben", "C-", "2");
        student1.inflateGrade();
        student1.boostAttendance();
        System.out.println(student1);
    }


}
