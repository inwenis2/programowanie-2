package date_29072018.designpatterns.templatemethod;

public enum Discount {
    HOLIDAY,
    REGULARCUSTOMER,
    MONTHLY
}
