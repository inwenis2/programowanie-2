package date_29072018.designpatterns.templatemethod;

public abstract class AbstractMoneyTransfer {

    public void peformTransfer(int amount, DiscountInterface discountInterface){
        prepareTransfer();
        addComission();
        sendMoney(amount + addComission() + addDiscount(discountInterface));
    }

    private int addDiscount(DiscountInterface discountInterface){
        return discountInterface.calculateDiscount();
    }

    protected abstract void prepareTransfer();
    protected abstract int addComission();
    void sendMoney(int amount){
        System.out.println("Transfering money..." + amount);
    }
}
