package date_29072018.designpatterns.templatemethod;

public class Runner {
    public static void main(String[] args) {
        PkoMoneyTransfer pkoMoneyTransfer = new PkoMoneyTransfer();
        MilleniumBankTransfer milleniumBankTransfer = new MilleniumBankTransfer();

        pkoMoneyTransfer.peformTransfer(100, new DiscountHoliday());
        milleniumBankTransfer.peformTransfer(100, new DiscounRegularCustomer());
    }
}
