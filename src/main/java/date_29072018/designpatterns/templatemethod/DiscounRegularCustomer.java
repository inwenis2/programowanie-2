package date_29072018.designpatterns.templatemethod;

public class DiscounRegularCustomer implements DiscountInterface{
    @Override
    public int calculateDiscount() {
        return 10;
    }
}
