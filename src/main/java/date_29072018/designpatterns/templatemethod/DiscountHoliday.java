package date_29072018.designpatterns.templatemethod;

public class DiscountHoliday implements DiscountInterface {

    @Override
    public int calculateDiscount() {
        return 5;
    }
}
