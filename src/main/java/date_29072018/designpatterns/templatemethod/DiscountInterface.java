package date_29072018.designpatterns.templatemethod;

public interface DiscountInterface {
    public int calculateDiscount();
}
