package date_29072018.designpatterns.templatemethod;

public class MilleniumBankTransfer extends AbstractMoneyTransfer {
    @Override
    protected void prepareTransfer() {
        System.out.println("Starting transfer from bank Millenium");
    }

    @Override
    protected int addComission() {
        return 15;
    }
}
