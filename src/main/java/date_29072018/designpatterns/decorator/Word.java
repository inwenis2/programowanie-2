package date_29072018.designpatterns.decorator;

public interface Word {

    public String display();
}
