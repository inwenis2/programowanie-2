package date_29072018.designpatterns.decorator;

public class Runner {
    public static void main(String[] args) {
        WordDecorator word = new Prefix(new Suffix(new NormalWord()));
        System.out.println(word.display());
    }
}
