package date_29072018.designpatterns.decorator;

public abstract class WordDecorator implements Word{

    private Word word;

    public Word getWord() {
        return word;
    }

    public WordDecorator(Word word){

        this.word = word;
    }

}
