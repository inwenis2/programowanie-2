package date_29072018.designpatterns.decorator;

public class Prefix extends WordDecorator{

    public Prefix(Word word) {
        super(word);
    }

    @Override
    public String display() {
        return "go" + getWord().display();
    }
}
