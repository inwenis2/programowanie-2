package date_29072018.designpatterns.decorator;

public class Suffix extends WordDecorator {

    public Suffix(Word word) {
        super(word);
    }

    @Override
    public String display() {
        return getWord().display() + "pozycja";
    }
}
