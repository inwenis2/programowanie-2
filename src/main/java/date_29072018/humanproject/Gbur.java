package date_29072018.humanproject;

public class Gbur extends Czlowiek {
    public Gbur(String name, String surname, int age) {
        super(name, surname, age);
    }

    @Override
    protected String przywitajSie() {
        return "Nie przeszkadzaj mi";
    }

    public static class GburBuilder {
        private String name;
        private String surname;
        private int age;

        public GburBuilder(String name, String surname) {
            this.name = name;
            this.surname = surname;
        }

        public GburBuilder firstName(String name) {
            this.name = name;
            return this;
        }

        public GburBuilder lastName(String surname) {
            this.surname = surname;
            return this;
        }

        public GburBuilder age(int age) {
            this.age = age;
            return this;
        }

        public Gbur build() {
            return new Gbur(name, surname, age);
        }
    }
}
