package date_29072018.humanproject;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class SpisLudnosci {
    List<Czlowiek> listOfPeople;

    public SpisLudnosci(String people){
        this.listOfPeople = new ArrayList<>();
        String[] peopleTab = people.split(";");
        for (int i = 0; i < peopleTab.length; i++){
            String[] temp = peopleTab[i].split(",");
            Czlowiek kulturalny = new Kulturalny.KulturalnyBuilder(temp[0], temp[1]).age(Integer.parseInt(temp[2])).build();
            listOfPeople.add(kulturalny);
        }
    }

    public List<Czlowiek> sortByName(String s, BiFunction<String, List<Czlowiek>, List<Czlowiek>> function){
       List<Czlowiek> listByName = new ArrayList<>();
/*         for (Czlowiek c : listOfPeople){
            if(s.equals(c.getName())){
                listByName.add(c);
            }
        }*/
        return function.apply(s, listByName);
    }

    public List<Czlowiek> sortByName2(String s, SortByName<Czlowiek> sortByName){
/*         for (Czlowiek c : listOfPeople){
            if(s.equals(c.getName())){
                listByName.add(c);
            }
        }*/
        return sortByName.sort(s, listOfPeople);
    }

    public List<Czlowiek> sortByName3(String nameToFind){
        return listOfPeople.stream()
                .filter(x -> x.getName().equals(nameToFind))
                .collect(Collectors.toList());
    }

}
