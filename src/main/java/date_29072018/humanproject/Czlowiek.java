package date_29072018.humanproject;

public class Czlowiek {
    private String name;
    private String surname;
    private int age;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Czlowiek{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }

    protected String przywitajSie(){
        return "Czesc";
    }
    protected String przedstawSie(){
        return przywitajSie() + "jestem " + name + " " + " " + surname + ". Mój wiek: " + age;
    }

    protected Czlowiek(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public static class CzlowiekBuilder{
        private String name;
        private String surname;
        private int age;

        public CzlowiekBuilder(String name, String surname) {
            this.name = name;
            this.surname = surname;
        }

        public CzlowiekBuilder firstName(String name) {
            this.name = name;
            return this;
        }

        public CzlowiekBuilder lastName(String surname) {
            this.surname = surname;
            return this;
        }

        public CzlowiekBuilder age(int age) {
            this.age = age;
            return this;
        }

        public Czlowiek build(){
            return new Czlowiek(name, surname, age);
        }
    }
}
