package date_29072018.humanproject;

public class Kulturalny extends Czlowiek {
    public Kulturalny(String name, String surname, int age) {
        super(name, surname, age);
    }

    @Override
    protected String przywitajSie() {
        return "Uszanowanko";
    }

    public static class KulturalnyBuilder {
        private String name;
        private String surname;
        private int age;

        public KulturalnyBuilder(String name, String surname) {
            this.name = name;
            this.surname = surname;
        }

        public KulturalnyBuilder firstName(String name) {
            this.name = name;
            return this;
        }

        public KulturalnyBuilder lastName(String surname) {
            this.surname = surname;
            return this;
        }

        public KulturalnyBuilder age(int age) {
            this.age = age;
            return this;
        }

        public Kulturalny build() {
            return new Kulturalny(name, surname, age);
        }
    }
}
