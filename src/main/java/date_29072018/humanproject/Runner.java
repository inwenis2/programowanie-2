package date_29072018.humanproject;

import java.util.ArrayList;

public class Runner {
    public static void main(String[] args) {
        Czlowiek k1 = new Kulturalny.KulturalnyBuilder("Adam", "Jadam").age(30).build();
        Czlowiek k2 = new Kulturalny.KulturalnyBuilder("Adam", "Slodowy").age(31).build();
        Czlowiek k3 = new Kulturalny.KulturalnyBuilder("Jan", "Kowalski").age(25).build();

        SpisLudnosci spisLudnosci = new SpisLudnosci("Adam,Jadam,30;Adam,Slodowy,31;Jan,Kowalski,21");
        System.out.println(spisLudnosci.listOfPeople.get(2));
        System.out.println(spisLudnosci.sortByName("Adam", (s, x) -> {
            for (Czlowiek c : spisLudnosci.listOfPeople){
                if(s.equals(c.getName())){
                    x.add(c);
                }
            }
            return x;
        } ).get(0));

        spisLudnosci.sortByName2("Adam", (s, x) -> {
            ArrayList<Czlowiek> array = new ArrayList<>();
            for (Czlowiek c : x){
                if(s.equals(c.getName())){
                    array.add(c);
                }
            }
            return array;
        });

        System.out.println(spisLudnosci.sortByName3("Adam"));
    }
}
