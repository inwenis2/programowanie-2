package date_29072018.humanproject;

public class CzlowiekMarionetka extends Czlowiek{

    private StrategiaPrzywitania strategiaPrzywitania;

    protected CzlowiekMarionetka(String name, String surname, int age, StrategiaPrzywitania strategiaPrzywitania) {
        super(name, surname, age);
        this.strategiaPrzywitania = strategiaPrzywitania;
    }

    @Override
    protected String przywitajSie() {
        return strategiaPrzywitania.przywitanie();
    }
    public static class CzlowiekMarionetkaBuilder {
        private String name;
        private String surname;
        private int age;
        private StrategiaPrzywitania strategiaPrzywitania;

        public CzlowiekMarionetkaBuilder(String name, String surname, StrategiaPrzywitania strategiaPrzywitania) {
            this.name = name;
            this.surname = surname;
            this.strategiaPrzywitania = strategiaPrzywitania;
        }

        public CzlowiekMarionetkaBuilder firstName(String name) {
            this.name = name;
            return this;
        }

        public CzlowiekMarionetkaBuilder lastName(String surname) {
            this.surname = surname;
            return this;
        }

        public CzlowiekMarionetkaBuilder age(int age) {
            this.age = age;
            return this;
        }

        public CzlowiekMarionetkaBuilder strategiaPrzywitania(StrategiaPrzywitania strategiaPrzywitania) {
            this.strategiaPrzywitania = strategiaPrzywitania;
            return this;
        }

        public CzlowiekMarionetka build() {
            return new CzlowiekMarionetka(name, surname, age, strategiaPrzywitania);
        }
    }
}
