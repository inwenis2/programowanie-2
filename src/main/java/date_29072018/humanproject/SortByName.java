package date_29072018.humanproject;

import java.util.List;

public interface SortByName<E> {
    List<E> sort(String s, List<E> list);
}
