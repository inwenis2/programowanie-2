package date_29072018.collections;

import java.util.*;

public class UniqueLetters {

    Map<String, Integer> memory = new HashMap<>();

    public int getUniqueLettersNumber(String word){
        if(memory.containsKey(word.toLowerCase())){
            return memory.get(word.toLowerCase());
        } else {
            Set<String> uniqueLetters = new HashSet<>();
            String[] words = word.toLowerCase().replaceAll(" ","").split("");
            uniqueLetters.addAll(Arrays.asList(words));
            memory.putIfAbsent(word.toLowerCase(), uniqueLetters.size());
            return uniqueLetters.size();
        }
    }
}
