package date_29072018.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LetterPosition {

    public static Map<String, List<Integer>> getLetterPosition(String word){
        Map<String, List<Integer>> letterPosition = new HashMap<>();
        String[] words = word.replaceAll(" ","").split("");
        for (int i = 0; i < words.length; i++){
            letterPosition.putIfAbsent(words[i], new ArrayList<Integer>());
            letterPosition.get(words[i]).add(i);
        }
        return letterPosition;
    }
}
