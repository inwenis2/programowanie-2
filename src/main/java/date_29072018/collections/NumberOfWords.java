package date_29072018.collections;

import java.util.HashMap;
import java.util.Map;

public class NumberOfWords {

    public Map<String, Integer> countingWords(String word){
        Map<String, Integer> numberOfWords = new HashMap<>();
        String[] words = word.split(" ");
        for(String s: words){
           if(numberOfWords.containsKey(s)){
               numberOfWords.put(s, numberOfWords.get(s)+1);
           }
           numberOfWords.put(s, 1);
        }
        return numberOfWords;
    }
}
