package date_29072018.additionalexercises;

public class Staircase {

    public static int possibleCombinations(int n){
        if (n == 0) {
            return 0;
        } else if (n == 1){
            return 1;
        } else if (n == 2){
            return 2;
        } else {
            return possibleCombinations(n-1) + possibleCombinations(n-2);
        }
    }
}
