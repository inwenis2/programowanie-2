package date_04082018;

public class NotificationService{
    private EmailService emailService;
    private PigeonService pigeonService;

    public NotificationService(EmailService emailService, PigeonService pigeonService) {
        this.emailService = emailService;
        this.pigeonService = pigeonService;
    }

    public void sendNotification (){
        if(emailService.isAvailable()){
            emailService.sendEmail("Wysyłam emaila");
        } else if (pigeonService.isAvailable()){
            pigeonService.sendMessage("Wysyłam gołębia");
        }
    }
}
