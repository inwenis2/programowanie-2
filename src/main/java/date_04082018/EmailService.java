package date_04082018;

public interface EmailService {
    public boolean isAvailable();
    public void sendEmail(String message);
}
