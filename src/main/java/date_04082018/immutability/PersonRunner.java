package date_04082018.immutability;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PersonRunner {
    public static void main(String[] args) {
        Person p1 = new Person("Adam", "Slodowy");
        Person p2 = new Person("Andrzej", "Kaminski");
        Person p3 = new Person("Pawel", "Siennicki");
        Person p4 = new Person("Mariusz", "Adamski");
        Person p5 = new Person("Kamil", "Przedni");
        Person p6 = new Person("Jan", "Kowalski");
        Person p7 = new Person("Adam", "Slodowy");

        Set<Person> personSet = new HashSet<>();
        personSet.add(p1);
        personSet.add(p2);
        personSet.add(p3);
        personSet.add(p4);
        personSet.add(p5);
        personSet.add(p6);

        System.out.println("Czy zawiera Adam Slodowy: " + personSet.contains(new Person("Adam", "Slodowy")));

        p6.setSurname("Nowak");

        System.out.println("Czy zawiera Jan Nowak: " + personSet.contains(p6));
        System.out.println("Cały zbiór:\n" + personSet);

        List<Person> personList = new ArrayList<>();
        personList.add(p1);
        personList.add(p2);
        personList.add(p3);
        personList.add(p4);
        personList.add(p5);
        personList.add(p6);
        personList.add(p7);

        System.out.println("Lista przed zmianą:\n" + personList);
        System.out.println("Lista bez duplikatów:\n" + uniquePersons(personList));

    }
    public static List<Person> uniquePersons(List<Person> personList){
        Set<Person> temp = new HashSet<>();
        List<Person> newPersonList = new ArrayList<>();
        temp.addAll(personList);
        newPersonList.addAll(temp);
        return newPersonList;
    }
}
