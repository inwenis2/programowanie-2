package date_04082018.immutability;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ImmutableList {
    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>(Arrays.asList(3, 5, 1, -4));
        System.out.println(sumAbsoluteValues(Collections.unmodifiableList(integerList)));
        System.out.println(sum(integerList));
    }

    public static int sum(List<Integer> integerList){
        return integerList.stream()
                .reduce(0, (x1, x2)-> x1 + x2);
    }

    public static void calculateAbsoluteValues(List<Integer> integerList){
        List<Integer> temp = new ArrayList<>();
        for (int i = 0; i < integerList.size(); i++){
            if(integerList.get(i) < 0){
                integerList.set(i,-integerList.get(i));
            }
        }
    }

    public static int sumAbsoluteValues (List<Integer> integerList){
        calculateAbsoluteValues(integerList);
        return sum(integerList);
    }
}
