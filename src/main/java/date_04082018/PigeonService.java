package date_04082018;

public interface PigeonService {
    public boolean isAvailable();
    public void sendMessage(String message);
}
