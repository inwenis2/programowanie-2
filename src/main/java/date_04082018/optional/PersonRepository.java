package date_04082018.optional;

import java.util.List;
import java.util.Optional;

public class PersonRepository {
    private List<Person> personList;

    public PersonRepository(List<Person> personList) {
        this.personList = personList;
    }

    public void addPerson(Person person){
        for(Person p : personList){
            if(p.getId() == person.getId()){
                throw new IllegalArgumentException("Nie można dodać osoby o tym samym ID");
            }
        }
        personList.add(person);
    }

    public Optional<Person> getPerson(int id){
        for (Person p : personList){
            if (p.getId()==id){
                return Optional.of(p);
            }
        }
        return Optional.empty();
    }

    public Optional<String> getName(int id){
//        if(getPerson(id).isPresent()){
//            return Optional.of(getPerson(id).get().getName());
//        } else {
//            return Optional.empty();
//        }

        return getPerson(id).map(Person::getName);
    }

    public Optional<String> getSurname(int id){
        if(getPerson(id).isPresent()){
            return Optional.of(getPerson(id).get().getSurname());
        } else {
            return Optional.empty();
        }
    }

    public Optional<Integer> getAge(int id){
        if(getPerson(id).isPresent()){
            return Optional.of(getPerson(id).get().getAge());
        } else {
            return Optional.empty();
        }
    }
}
