package date_21072018;

import org.junit.Test;

import static org.junit.Assert.*;

public class PalindromTest {

    @Test
    public void isPalindromTest() {
        boolean result = Palindrom.isPalindrom("KobyLa ma mAlY bok");
        assertTrue(result);
    }

    @Test
    public void isNotPalindromTest(){
        boolean result = Palindrom.isPalindrom("tablica");
        assertFalse(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullTest(){
        boolean result = Palindrom.isPalindrom(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyStringTest(){
        boolean result = Palindrom.isPalindrom("");
    }

}