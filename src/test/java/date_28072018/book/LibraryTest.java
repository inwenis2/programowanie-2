package date_28072018.book;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class LibraryTest {
    List<Book> bookList;
    Library library;

    @Before
    public void setup(){
        bookList = new ArrayList<>();
        bookList.add(new Book("Henryk Sienkiewicz", "W pustyni i w puszczy", 39, 2017));
        bookList.add(new Book("Henryk Sienkiewicz", "Krzyżacy", 30, 2015));
        bookList.add(new Book("Philip K. Dick", "Blade Runner. Czy androidy marzą o elektrycznych owcach?", 49, 2013));
        bookList.add(new Book("Philip K. Dick", "Człowiek z wysokiego zamku", 30, 2017));
        bookList.add(new Book("Katarzyna Bonda", "Sprawa Niny Frank", 41, 2017));
        bookList.add(new Book("Katarzyna Bonda", "Tylko martwi nie kłamią", 39, 2017));
/*        bookList.add(new Book("Zygmunt Miłoszewski", "Uwikłanie", 41, 2017));
        bookList.add(new Book("Zygmunt Miłoszewski", "Ziarno prawdy", 39, 2017));*/

        library = new Library(bookList);
    }

    @Test
    public void testListOfAuthors(){
        String[] expected = {"Henryk Sienkiewicz", "Henryk Sienkiewicz", "Philip K. Dick", "Philip K. Dick", "Katarzyna Bonda", "Katarzyna Bonda"};
        assertArrayEquals(expected, library.getAuthor().toArray());
    }

    @Test
    public void testListOfTitles(){
        String[] expected = {"W pustyni i w puszczy", "Krzyżacy", "Blade Runner. Czy androidy marzą o elektrycznych owcach?", "Człowiek z wysokiego zamku", "Sprawa Niny Frank", "Tylko martwi nie kłamią"};
        assertArrayEquals(expected, library.getTitles().toArray());
    }

    @Test
    public void testListOfPrices(){
        Integer[] expected = {39, 30, 49, 30, 41, 39};
        assertArrayEquals(expected, library.getPrices().toArray());
    }

    @Test
    public void booksOfParticularAuthor(){
        Book[] expected = {new Book("Henryk Sienkiewicz", "W pustyni i w puszczy", 39, 2017), (new Book("Henryk Sienkiewicz", "Krzyżacy", 30, 2015))};
        assertArrayEquals(expected, library.booksOfParticularAuthor("Henryk Sienkiewicz").toArray());
    }

    @Test
    public void booksOfParticularTitle(){
        Book[] expected = {new Book("Henryk Sienkiewicz", "W pustyni i w puszczy", 39, 2017)};
        assertArrayEquals(expected, library.booksOfParticularTitle("W pustyni i w puszczy").toArray());
    }

    @Test
    public void booksOfParticularPrice(){
        Book[] expected = {new Book("Henryk Sienkiewicz", "W pustyni i w puszczy", 39, 2017), new Book("Katarzyna Bonda", "Tylko martwi nie kłamią", 39, 2017)};
        assertArrayEquals(expected, library.booksOfParticularPrice(39).toArray());
    }

    @Test
    public void booksOfPriceRange(){
        Book[] expected = {new Book("Henryk Sienkiewicz", "W pustyni i w puszczy", 39, 2017), new Book("Henryk Sienkiewicz", "Krzyżacy", 30, 2015), new Book("Philip K. Dick", "Człowiek z wysokiego zamku", 30, 2017), new Book("Katarzyna Bonda", "Tylko martwi nie kłamią", 39, 2017)};
        assertArrayEquals(expected, library.booksOfPriceRange(30, 39).toArray());
    }

    @Test
    public void sumOfAllBooksPrice(){
        assertEquals(228, library.getPriceSum());
    }

    @Test
    public void sumOfAllBooksPrice2(){
        assertEquals(228, library.getPriceSum2());
    }

    @Test
    public void getSumPriceByAuthorTest(){
        assertEquals(39, library.getPriceSumByAnAuthor("Henryk Sienkiewicz", "Krzyżacy"));
    }

    @Test
    public void booksByAYear(){
        Book[] expected = {new Book("Henryk Sienkiewicz", "W pustyni i w puszczy", 39, 2017), new Book("Philip K. Dick", "Człowiek z wysokiego zamku", 30, 2017), new Book("Katarzyna Bonda", "Sprawa Niny Frank", 41, 2017), new Book("Katarzyna Bonda", "Tylko martwi nie kłamią", 39, 2017)};
        assertArrayEquals(expected, library.booksByAYear(2017).toArray());
    }

    @Test
    public void booksByAnAuthorAndYearMaxPriceTest(){
        Book[] expected = {new Book("Katarzyna Bonda", "Sprawa Niny Frank", 41, 2017), new Book("Katarzyna Bonda", "Tylko martwi nie kłamią", 39, 2017)};
        assertArrayEquals(expected, library.booksByAnAuthorAndYearMaxPrice("Katarzyna Bonda", 2017, 41).toArray());
    }

    @Test
    public void testStringOfAuthors(){
        String expected = "- Henryk Sienkiewicz\n- Henryk Sienkiewicz\n- Philip K. Dick\n- Philip K. Dick\n- Katarzyna Bonda\n- Katarzyna Bonda\n- ";
        assertEquals(expected, library.getAuthorsInAParticularWay());
    }
}