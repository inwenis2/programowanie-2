package date_29072018.additionalexercises;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static date_29072018.additionalexercises.Staircase.possibleCombinations;
import static org.junit.Assert.*;
@RunWith(Parameterized.class)
public class StaircaseTest {

    @Parameterized.Parameters
    public static Collection<Integer[]> data() {
        return Arrays.asList(new Integer[][] {{0,0}, {1,1}, {2,2}, {3,3}, {4,5}, {5,8}, {12, 233}});
    }

    @Parameterized.Parameter
    public int input;
    @Parameterized.Parameter(1)
    public int expected;

    @Test
    public void possibleCombinationsTest() {
        assertEquals(expected, possibleCombinations(input));
    }
}