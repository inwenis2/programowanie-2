package date_29072018.humanproject;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GburTest {

    Czlowiek gbur;

    @Before
    public void setUp(){
        gbur = new Gbur.GburBuilder("Marian", "Peszek").age(31).build();
    }

    @Test
    public void przywitajSie(){
        assertEquals("Nie przeszkadzaj mi",gbur.przywitajSie());
    }

}