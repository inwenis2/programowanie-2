package date_29072018.humanproject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
@RunWith(MockitoJUnitRunner.class)
public class CzlowiekMarionetkaTest {


    @Mock
    StrategiaPrzywitania strategiaPrzywitania;

    Czlowiek czlowiekMarionetka;

    @Before
    public void setUp(){
        when(strategiaPrzywitania.przywitanie()).thenReturn("Siema");
        czlowiekMarionetka = new CzlowiekMarionetka.CzlowiekMarionetkaBuilder("Karol", "Milosz", strategiaPrzywitania).age(43).build();
    }

    @Test
    public void przywitajSie(){
        assertEquals("Siema", czlowiekMarionetka.przywitajSie());
    }

    @Test
    public void shouldCallStrategiaPrzywitaniaSiemaWhenCalledPrzywitajSie(){
        czlowiekMarionetka.przywitajSie();
        verify(strategiaPrzywitania).przywitanie();
    }
}