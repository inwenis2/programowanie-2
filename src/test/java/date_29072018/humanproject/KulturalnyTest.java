package date_29072018.humanproject;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class KulturalnyTest {
    Czlowiek kulturalny;

    @Before
    public void setUp(){
        kulturalny = new Kulturalny.KulturalnyBuilder("Jan", "Kowalski").age(25).build();
    }

    @Test
    public void przywitajSieTest(){
        assertEquals("Uszanowanko",kulturalny.przywitajSie());
    }

}