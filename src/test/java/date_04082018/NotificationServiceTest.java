package date_04082018;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTest {

    @Mock
    EmailService emailService;
    @Mock
    PigeonService pigeonService;
    @InjectMocks
    NotificationService notificationService;

    @Test
    public void shouldSendPigeonNotification() {
        Mockito.when(pigeonService.isAvailable()).thenReturn(true);
        notificationService.sendNotification();
        Mockito.verify(pigeonService).sendMessage("Wysyłam gołębia");
    }

    @Test
    public void shouldSendEmailNotification() {
        Mockito.when(emailService.isAvailable()).thenReturn(true);
        notificationService.sendNotification();
        Mockito.verify(emailService).sendEmail("Wysyłam emaila");
    }
}